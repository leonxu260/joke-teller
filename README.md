# Joke Teller

An app that combines a Joke API, and a speech-to-text API

## Getting Started

These instructions will give you a copy of the project up and running on
your local machine for development and testing purposes. See deployment
for notes on deploying the project on a live system.

## Built With

  - [JSDoc](https://jsdoc.app/) - Used
    for JSDoc commenting
  - [GNU General Public License](LICENSE.md) - Used to choose
    the license

## Authors

  - [Leon Xu](https://gitlab.com/leonxu260)

## License

This project is licensed under the [GNU General Public License](LICENSE.md) - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments
- [Text-to-speech (TTS) API](http://www.voicerss.org/api/)
- [JokeAPI](https://sv443.net/jokeapi/v2/)
